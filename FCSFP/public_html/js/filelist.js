/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $(function () {
        var user = $.cookie('usr_cookie');
        var $files = $('#list');

        var fileTemp = '<tbody><tr><td>{{file}}</td>' + '<td>{{added}}</td>' + '<td>{{type}}</td>' +
                '<td><a href="http://localhost:8080/StudentShare/api/file/txt/{{file}}"class="btn btn-success btn-sm">Download</a>\n\
                <a id="share" data-toggle="modal" data-target="#exampleModal" data-id= "{{file}}" class="btn btn-success btn-sm">Share\n\
                </a><a  id="dele"  data-id= "{{file}}" class="btn btn-success btn-sm ">Delete\n\
                            </a></td>' + '</tr></tbody>';

        function addFile(file) {
            $("#fileli").append(Mustache.render(fileTemp, file));
        }

        $.ajax({
            type: 'GET',
            url: "http://localhost:8080/StudentShare/api/user/list/" + user,
            crossDomain: true,
            dataType: "json",
            success: function (files) {
                $.each(files, function (i, file) {
                    addFile(file);
                });
            },
            error: function () {
                alert("not working");
            }
        });
    });
    $(document).on('click', '#share', function () {
        var myFileId = $(this).data('id');
        $(".modal-body #id").val(myFileId);
    });

    
    $("#shreuser").click(function(){
        var id = $('#id').val();
        var user = $('#userd').val();
        var current = $.cookie('usr_cookie');
        var JsonObject = {"id":id,"user":user,"current":current};
        
        var data = JSON.stringify(JsonObject);
       
        $.ajax({
            crossDomain: true,
            cache: false,
            type:"POST",
            url:"http://localhost:8080/StudentShare/api/user/share",
            data: data,
            dataType: "text",
            contentType: "application/json",
            
            success: function(response){
                alert("success" + response);
                alert($.cookie('usr_cookie'));
            },
            error: function(response){
                alert("error:" + JSON.stringify(response));
                
            }
            
        });
        
    });
    
    $(document).on('click', '#dele', function () {
        var file = $('td:first', $(this).parents('tr')).text();
        var name = $.cookie('usr_cookie');
        alert(file);
        var JsonObject = {"name":name,"file":file};
        
        var data = JSON.stringify(JsonObject);
       
        $.ajax({
            crossDomain: true,
            cache: false,
            type:"POST",
            url:"http://localhost:8080/StudentShare/api/user/delete",
            data: data,
            dataType: "text",
            contentType: "application/json",
            
            success: function(response){
                alert("success" + response);
                alert($.cookie('usr_cookie'));
            },
            error: function(response){
                alert("error:" + JSON.stringify(response));
                
            }
            
        });
        
    });
});


