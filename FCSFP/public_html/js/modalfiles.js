/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    var a = parent.document.URL.substring(parent.document.URL.indexOf('?'), parent.document.URL.length + 1);
    var b = a.replace('?', '');
    alert(b);

    $(function () {
        var user = $.cookie('usr_cookie');
        var $teams = $('#teamli');

        var teamTemp = '<tbody><tr><td id="hh"> {{file}}  </td>' + '<td>{{team.createdby}} </td>' + '<td> </td>'+
                '<td>\n\
                      <a id="team" class="btn btn-success btn-sm" href="http://localhost:8080/StudentShare/api/file/txt/{{file}}">Download\n\
                      </a><a id="share" data-toggle="modal" data-target="#exampleModal" data-id= "{{file}}" class="btn btn-success btn-sm">Share\n\
                      </a><a id="dele" class="btn btn-success btn-sm">Delete\n\
                      </a></td>' + '</tr></tbody>';

        function addFile(team) {
            $("#teamli").append(Mustache.render(teamTemp, team));
        }

        $.ajax({
            type: 'GET',
            url: "http://localhost:8080/StudentShare/api/TeamFile/list/" + user+"/"+b,
            crossDomain: true,
            dataType: "json",
            success: function (teams) {
                $.each(teams, function (i, UserInTeam) {
                    addFile(UserInTeam);
                });
            },
            error: function () {
                alert("not working");
            }
            
            
        });
    });
    
    $(document).on('click', '#share', function () {
        var myFileId = $(this).data('id');
        $(".modal-body #id").val(myFileId);
    });

    $("#shreuser").click(function(){
        var id = $('#id').val();
        var user = $('#userd').val();
        var current = $.cookie('usr_cookie');
        var JsonObject = {"id":id,"user":user,"current":current};
        
        var data = JSON.stringify(JsonObject);
       
        $.ajax({
            crossDomain: true,
            cache: false,
            type:"POST",
            url:"http://localhost:8080/StudentShare/api/user/share",
            data: data,
            dataType: "text",
            contentType: "application/json",
            
            success: function(response){
                alert("success" + response);
                alert($.cookie('usr_cookie'));
            },
            error: function(response){
                alert("error:" + JSON.stringify(response));
                
            }
            
        });
        
    });
    
    $(document).on('click', '#dele', function () {
        
        var file = $('td:first', $(this).parents('tr')).text();
        var user = $.cookie('usr_cookie');
        alert(file);
        var JsonObject = {"user":user,"file":file,"b":b};
        
        var data = JSON.stringify(JsonObject);
       
        $.ajax({
            crossDomain: true,
            cache: false,
            type:"POST",
            url:"http://localhost:8080/StudentShare/api/TeamFile/delete",
            data: data,
            dataType: "text",
            contentType: "application/json",
            
            success: function(response){
                alert("success" + response);
                alert($.cookie('usr_cookie'));
            },
            error: function(response){
                alert("error:" + JSON.stringify(response));
                
            }
            
        });
        
    });
});
