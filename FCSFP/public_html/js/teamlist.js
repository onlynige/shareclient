/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $(function () {
        var user = $.cookie('usr_cookie');
        var $teams = $('#teamli');

        var teamTemp = '<tbody><tr><td id="hh">{{team.teamname}}</td>'+'<td>{{team.createdby}}</td>' + '<td> </td>'+
                '<td>\n\
                      <a id="team" class="btn btn-success btn-sm" href="teamfiles.html?{{team.teamname}}">View\n\
                      </a><button id="dele" data-toggle="modal" data-id="{{team.teamname}}" data-target="#exampleModal" class="btn btn-success btn-sm" >Delete\n\
                      </button></td>' + '</tr></tbody>';

        function addFile(team) {
            $("#teamli").append(Mustache.render(teamTemp, team));
        }

        $.ajax({
            type: 'GET',
            url: "http://localhost:8080/StudentShare/api/team/list/" + user,
            crossDomain: true,
            dataType: "json",
            success: function (teams) {
                $.each(teams, function (i, UserInTeam) {
                    addFile(UserInTeam);
                });
            },
            error: function () {
                alert("not working");
            }
        });
    });
    
    $(document).on('click', '#dele', function () {
        var myFileId = $(this).data('id');
        $(".modal-body #id").val(myFileId);
    });

    $("#del").click(function(){
        var id = $('#id').val();
        var user = $(this).closest('tr').children('td.two').text();
        alert(user);
        var current = $.cookie('usr_cookie');
        var JsonObject = {"id":id,"current":current};
        
        var data = JSON.stringify(JsonObject);
       
        $.ajax({
            crossDomain: true,
            cache: false,
            type:"POST",
            url:"http://localhost:8080/StudentShare/api/team/deleteuser",
            data: data,
            dataType: "text",
            contentType: "application/json",
            
            success: function(response){
                alert("success" + response);
                alert($.cookie('usr_cookie'));
            },
            error: function(response){
                alert("error:" + JSON.stringify(response));
                
            }
            
        });
        
    });
    
});
