/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $("#add-address").click(function (e) {
        e.preventDefault();
        var numberOfAddresses = $("#form1").find("input[name^='data[address]']").length;
        var label = '<label for="data[address][' + numberOfAddresses + ']">Address ' + (numberOfAddresses + 1) + '</label> ';
        var input = '<input type="text" name="data[address][' + numberOfAddresses + ']" id="data[address][' + numberOfAddresses + ']" />';
        var removeButton = '<button class="remove-address">Remove</button>';
        var html = "<div class='address'>" + label + input + removeButton + "</div>";
        $("#form1").find("#add-address").before(html);


    });
});

$(document).on("click", ".remove-address", function (e) {
    e.preventDefault();
    $(this).parents(".address").remove();
    //update labels
    $("#form1").find("label[for^='data[address]']").each(function () {
        $(this).html("Address " + ($(this).parents('.address').index() + 1));
    });
});

$(document).ready(function(){
    $('#add').click(function(){
        window.location.replace("user.html");
    });
});

$(document).ready(function () {
    $("#teamcreate").click(function () {
        var team = $('#team').val();
        var user = $.cookie('usr_cookie');

        var JsonObject = {"team": team, "user": user};


        var data = JSON.stringify(JsonObject);

        $.ajax({
            crossDomain: true,
            cache: false,
            type: "POST",
            url: "http://localhost:8080/StudentShare/api/team/createteam",
            data: data,
            dataType: "text",
            contentType: "application/json",
            success: function (response) {
                if (response === 'team created') {
                    alert("success" + response);
                    window.location.replace("user.html");
                    $.cookie('team', team, {path: '/', expires: 1});
                }
            },
            error: function (response) {
                alert("error:" + JSON.stringify(response));
            }

        });

    });
});