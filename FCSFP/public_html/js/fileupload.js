/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#uploadf').click(submitForm);
});

function submitForm() {
    var file = $('input[name="upload"').get(0).files[0];
    var user = $.cookie('usr_cookie');
    var formData = new FormData();
    formData.append('session', user);
    formData.append('file', file);

    $.ajax({
        crossDomain: true,
        url: "http://localhost:8080/StudentShare/api/file/upload",
        type: "POST",
        data: formData,
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,
        xhr: function () {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            return myXhr;
        },
        // beforeSend: beforeSendHandler,
        success: function (data) {
            alert(data);
        },
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', progress, false);
            } else {
                console.log('Upload progress is not supported.');
            }
            return myXhr;
        }
    });
}
function progress(e) {
    if (e.lengthComputable) {
        $('#progress_percent').text(Math.floor((e.loaded * 100) / e.total));
        $('progress').attr({value: e.loaded, max: e.total});
    }
}

